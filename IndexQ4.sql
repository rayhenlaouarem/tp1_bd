CREATE BITMAP INDEX MONTH_GROUP_BJIX
ON ACTVARS (TIMELEVEL.MONTH_LEVEL , PRODLEVEL.GROUP_LEVEL)
FROM ACTVARS , TIMELEVEL, PRODLEVEL
WHERE ACTVARS.TIME_LEVEL=TIMELEVEL.TID
AND  ACTVARS.PRODUCT_LEVEL= PRODLEVEL.CODE_LEVEL ;

CREATE BITMAP INDEX DAY_QUARTER_BJIX
ON ACTVARS (TIMELEVEL.DAY_LEVEL , TIMELEVEL.QUARTER_LEVEL)
FROM ACTVARS , TIMELEVEL
WHERE ACTVARS.TIME_LEVEL=TIMELEVEL.TID;
